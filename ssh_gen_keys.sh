#!/bin/bash
mkdir files/openssh
ssh-keygen -f files/openssh/ssh_host_rsa_key -t rsa -b 4096 -P ''
ssh-keygen -f files/openssh/ssh_host_ecdsa_key -t ecdsa -P ''
ssh-keygen -f files/openssh/ssh_host_ed25519_key -t ed25519 -P ''
