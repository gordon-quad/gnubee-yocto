DEPENDS:remove = "udev"
EXTRA_OEMESON = "\
    -Dudevrulesdir=/dummy \
"

do_install:append() {
    rm -rf ${D}/dummy
    sed -i 's/#user_allow_other/user_allow_other/' ${D}${sysconfdir}/fuse.conf
}
