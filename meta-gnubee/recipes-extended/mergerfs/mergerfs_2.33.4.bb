SUMMARY = "A featureful union filesystem"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://LICENSE;md5=0d255c27f4673c7ab587adfe7b618f9c"

SRCREV = "77e6f400b5ce6b06b20e5d937b6cfbf2f3053eb5"
SRC_URI = "git://github.com/trapexit/mergerfs.git;protocol=https;branch=master \
    file://0001-nostrip.patch \
"

S = "${WORKDIR}/git"

inherit pkgconfig

DEPENDS += "fuse3"

# Workaround for QA
TARGET_CC_ARCH += "${LDFLAGS}"

do_compile () {
    oe_runmake DESTDIR="${D}" PREFIX="${prefix}"
}

do_install () {
    oe_runmake DESTDIR="${D}" PREFIX="${prefix}" \
        install
}

