SUMMARY = "Optional tools to help manage data in a mergerfs pool"
LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://LICENSE;md5=fbaef4d4477ff6cc657d9f8ecb210651"

SRCREV = "${AUTOREV}"
SRC_URI = "git://github.com/trapexit/mergerfs-tools.git;protocol=https;branch=master"

S = "${WORKDIR}/git"

do_compile () {
    true
}

do_install () {
    oe_runmake DESTDIR="${D}" PREFIX="${prefix}" \
        install
}

RDEPENDS:${PN} += "bash"

