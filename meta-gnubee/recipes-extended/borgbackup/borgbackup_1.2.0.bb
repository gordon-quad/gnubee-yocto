SUMMARY = "Deduplicating backup program with compression and authenticated encryption"
LICENSE = "BSD-3-Clause"
LIC_FILES_CHKSUM = "file://LICENSE;md5=4b81f01bf74e454e57a4097a5ab66874"

DEPENDS = " \
    lz4 \
    openssl \
    acl \
    ${PYTHON_PN}-pkgconfig-native \
    ${PYTHON_PN}-setuptools-scm-native \
"

inherit pypi setuptools3 pkgconfig

SRC_URI[sha256sum] = "e39a5547902ef456101aa4c779fa66b345bda70d16788e8bd18e458f93af7f67"

PYPI_PACKAGE = "borgbackup"

RDEPENDS:${PN} += "\
    ${PYTHON_PN}-fuse3 \
    ${PYTHON_PN}-msgpack \
    ${PYTHON_PN}-packaging \
"

CLEANBROKEN="1"

