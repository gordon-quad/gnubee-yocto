do_install:append () {
    update-rc.d -r ${D} -f checkroot.sh remove
    update-rc.d -r ${D} -f bootmisc.sh remove
    update-rc.d -r ${D} -f hwclock.sh remove
    update-rc.d -r ${D} -f mountall.sh remove
}
