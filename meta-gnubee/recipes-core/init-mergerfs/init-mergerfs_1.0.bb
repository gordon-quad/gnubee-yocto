SUMMARY = "mergerfs init script"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit update-rc.d

INITSCRIPT_NAME = "mergerfs"
INITSCRIPT_PARAMS = "start 80 2 3 4 5 . stop 01 0 6 1 ."

SRC_URI = "file://init"

S = "${WORKDIR}"

do_install () {
    install -d ${D}${sysconfdir}/init.d
    install -m 0755 ${WORKDIR}/init ${D}${sysconfdir}/init.d/mergerfs
}

RDEPENDS:${PN} = "mergerfs"
