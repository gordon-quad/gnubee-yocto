SRC_URI += " \
    file://cron.cfg \
    file://ntp.cfg \
    file://hwclock.cfg \
    file://flashcp.cfg \
    file://busybox-ntpd \
"

DEPENDS:append = " update-rc.d-native"

PACKAGES =+ "${PN}-ntpd"

FILES:${PN}-ntpd = "${sysconfdir}/init.d/busybox-ntpd ${sysconfdir}/ntp.conf"

CONFFILES:${PN}-ntpd = "${sysconfdir}/ntp.conf"

INITSCRIPT_PACKAGES:append = " ${PN}-ntpd"
INITSCRIPT_NAME:${PN}-ntpd = "busybox-ntpd"
INITSCRIPT_PARAMS:${PN}-ntpd = "start 20 2 3 4 5 . stop 60 0 6 1 ."

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/files:"

do_install:append () {
    sed -i 's/DESTINATION=file/DESTINATION=buffer/' ${D}/etc/syslog-startup.conf
}

do_install:append () {
	if grep -q "CONFIG_NTPD=y" ${B}/.config; then
		install -m 0755 ${WORKDIR}/busybox-ntpd ${D}${sysconfdir}/init.d/
		[ -n "${NTP_SERVER}" ] && echo "server ${NTP_SERVER}" >${D}/etc/ntp.conf
	fi
}

