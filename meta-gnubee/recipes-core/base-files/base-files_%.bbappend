FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

do_install:append () {
    install -d ${D}/mnt
    install -d ${D}/mnt/stor0
    install -d ${D}/mnt/stor1
    install -d ${D}/mnt/stor2
    install -d ${D}/mnt/stor3
    install -d ${D}/mnt/stor4
    install -d ${D}/mnt/stor5
    install -d ${D}/mnt/merge
}
