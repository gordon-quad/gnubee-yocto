DESCRIPTION = "Tiny image for GnuBee capable of sshfs, mergerfs and borg"

VIRTUAL-RUNTIME_dev_manager = "busybox-mdev"

PACKAGE_INSTALL = "gnubee-init rc-local packagegroup-core-boot openssh openssh-sftp-server macchanger ${VIRTUAL-RUNTIME_base-utils} ${VIRTUAL-RUNTIME_dev_manager} base-passwd ${ROOTFS_BOOTSTRAP_INSTALL} wireguard-tools atop python3 hdparm msmtp smartmontools borgbackup mergerfs mergerfs-tools init-mergerfs xfsprogs init-wireguard init-macchanger ssh-custom-keys busybox-ntpd"

IMAGE_FEATURES = ""

export IMAGE_BASENAME = "core-image-gnubee"
IMAGE_NAME_SUFFIX ?= ""
IMAGE_LINGUAS = ""

LICENSE = "MIT"

IMAGE_FSTYPES = "${INITRAMFS_FSTYPES}"

inherit core-image extrausers
EXTRA_USERS_PARAMS = " \
    usermod -p '' root ; \
    useradd -u 1000 -p '' -m gordon ; \
"

IMAGE_ROOTFS_SIZE = "8192"
IMAGE_ROOTFS_EXTRA_SPACE = "0"

COMPATIBLE_HOST = "(mipsel|x86_64)-.*-linux"

