SUMMARY = "Macchanger plugin for ifupdown"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

RDEPENDS:${PN} = "init-ifupdown macchanger"

S = "${WORKDIR}"

do_configure() {
    :
}

do_compile() {
    :
}

do_install() {
    tmp="${MACCHANGER_MACS}"
    [ -z $tmp ] && exit 0
    install -d ${D}${sysconfdir}/network/if-pre-up.d
    echo '#!/bin/sh' >${D}${sysconfdir}/network/if-pre-up.d/macchanger
    chmod +x ${D}${sysconfdir}/network/if-pre-up.d/macchanger
    for i in $tmp
    do
        iiface=`echo ${i} | sed -e 's/=.*//'`
        imac=`echo ${i} | sed -e 's/^.*=//'`

        echo "if test \"x\$IFACE\" = x$iiface ; then" >>${D}${sysconfdir}/network/if-pre-up.d/macchanger
        echo "    macchanger -m $imac \$IFACE" >>${D}${sysconfdir}/network/if-pre-up.d/macchanger
        echo "fi" >>${D}${sysconfdir}/network/if-pre-up.d/macchanger
    done
}

FILES:${PN} = "/etc/network/if-pre-up.d/macchanger"
