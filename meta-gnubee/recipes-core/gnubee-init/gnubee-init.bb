SUMMARY = "Gnubee init"
DESCRIPTION = "Basic init system for gnubee-tiny"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

PR = "r2"

RDEPENDS:${PN} = "busybox"

S = "${WORKDIR}"

do_configure() {
    :
}

do_compile() {
    :
}

do_install() {
    install -d ${D}/dev
    mknod -m 622 ${D}/dev/console c 5 1
    mknod -m 666 ${D}/dev/null c 1 3
}

FILES:${PN} = "/init /dev /dev/console /dev/null"
RCONFLICTS:${PN} = "systemd"
