SUMMARY = "Wireguard init script"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

inherit update-rc.d

FILESEXTRAPATHS:prepend = "${TOPDIR}/../files/wireguard:"

INITSCRIPT_NAME = "wireguard"
INITSCRIPT_PARAMS = "start 02 2 3 4 5 . stop 79 0 6 1 ."

SRC_URI = "file://init \
           file://wireguard.conf \
"

S = "${WORKDIR}"

do_install () {
    install -d ${D}${sysconfdir}/init.d
    install -m 0700 -d ${D}${sysconfdir}/wireguard
    install -m 0755 ${WORKDIR}/init ${D}${sysconfdir}/init.d/wireguard
    install -m 0400 ${WORKDIR}/wireguard.conf ${D}${sysconfdir}/wireguard/wg0.conf
}

PACKAGE_ARCH = "${MACHINE_ARCH}"
RDEPENDS:${PN} = "netbase"
RCONFLICTS:${PN} = "netbase (< 1:5.0)"

CONFFILES:${PN} = "${sysconfdir}/wireguard.conf"
