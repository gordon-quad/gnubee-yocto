SUMMARY = "Python 3 bindings for libfuse 3 with asynchronous API"
LICENSE = "LGPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=381bf10e75692c1b3f58ab2b033ef0a4"

DEPENDS = "fuse3"

inherit pkgconfig pypi setuptools3

SRC_URI[sha256sum] = "22d146dac59a8429115e9a93317975ea54b35e0278044a94d3fac5b4ad5f7e33"

PYPI_PACKAGE = "pyfuse3"

#RDEPENDS:${PN} += "\
#    fuse3 \
#"

BBCLASSEXTEND = "native nativesdk"

CLEANBROKEN="1"

