require linux.inc

DESCRIPTION = "Gnubee Linux kernel fork"

COMPATIBLE_MACHINE = "gnubee"

PV = "5.15.12"
PR = "r1"
SRCREV = "2d92ed169a28a8402991204c8ef6b169f6bf971b"

MACHINE_KERNEL_PR:append = "a"

SRC_URI += "git://github.com/neilbrown/linux;branch=gnubee/v5.15;protocol=https \
        file://0001-wraptest-fix.patch \
        file://defconfig \
        "
LIC_FILES_CHKSUM = "file://COPYING;md5=6bc538ed5bd9a7fc9398086aedcd7e46"

INITRAMFS_IMAGE = "core-image-gnubee"

S = "${WORKDIR}/git"

